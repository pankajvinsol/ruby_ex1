# Public: Extended String Class to count no of character in an Hash
#
# Examples
#
#   # "lol".count_character
#   #=> {"l"=>2, "o"=>1}
# return hash having character count as value and character as key

class String
  def character_count
    count = Hash.new(0)
    each_char { |char| count[char] += 1 }
    count
  end
end

