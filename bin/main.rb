# Public: Count no of each character in input line
#
# Examples
#
#   ruby main.rb
#   # Enter a line: qwertyuiQAZWSXEDC
#   # {"q"=>1, "w"=>1, "e"=>1, "r"=>1, "t"=>1, "y"=>1, "u"=>1, "i"=>1, "Q"=>1, "A"=>1, "Z"=>1, "W"=>1, "S"=>1, "X"=>1, "E"=>1, "D"=>1, "C"=>1}
#	 
# print hash having no of each character

require_relative "../lib/string.rb"
print "Enter a line: "
line = gets.chomp.character_count
p line

